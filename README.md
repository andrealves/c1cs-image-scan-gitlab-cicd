# c1cs-image-scan-gitlab-cicd

Example stages to:

1) Queue a container image scan job

2) Generate a PDF file for that scan report as a build artifact

3) Upload the PDF file to an S3 bucket


## Requirements

1) Working and accessible from Gitlab runners Trend Micro Smart Check deployment (a). 

2) Smart Check deployment must have a valid certificate (b) and pre-registry scanning enabled (c).


  (a) https://cloudone.trendmicro.com/docs/container-security/sc-integrate/#integrate-with-deep-security-smart-check

  (b) https://cloudone.trendmicro.com/docs/container-security/sc-service-cert/#replace-the-service-certificate

  (c) https://cloudone.trendmicro.com/docs/container-security/pre-registry-scanning/


## How to use

1) Clone this repo;

2) Add the following Environment Variables to your Gitlab CI/CD Pipeline:

|Variable|Purpose                      |
|--------|-----------------------------|
|AWS_ACCESS_KEY_ID|Access Key ID for an AWS IAM User with programatic permissions for ECR_Read and S3_PUT|
|AWS_S3_BUCKET|Name of an S3 bucket to save Scan PDF Reports to. Example value: my-s3-bucket|
|AWS_SECRET_ACCESS_KEY|Secret Access Key for an AWS IAM User with programatic permissions for ECR_Read and S3_PUT|
|DSSC_FINDINGS_THRESHOLD|Threshold for build failure according to number of findings for this image scan. Example value:  '{"malware": 1, "vulnerabilities": { "defcon1": 5, "critical": 10, "high": 20 }, "contents": { "defcon1": 1, "critical": 5, "high": 20 }, "checklists": { "defcon1": 1, "critical": 10, "high": 20 }}'|
|DSSC PASS|Password for Trend Micro Smart Check|
|DSSC_PREREGISTRY_HOST|FQDN for the Smart Check deployment with Pre-Registry enabled. Example: smartcheck.example.com:5000 (5000 is the default pre-registry port)|
|DSSC_PREREGISTRY_PASSWORD|Password for the Pre-Registry user|
|DSSC_PREREGISTRY_USER|Username for the Pre-Registry user|
|DSSC_RESULTS_FILE|Value should be results.json |
|DSSC_URL|FQDN for the Smart Check deployment. Example: smartcheck.example.com|
|DSSC_USER|Username for Trend Micro Smart Check|
|ECR_REPO|ECR Repository name. Example: 01234567890123.dkr.ecr.us-east-1.amazonaws.com|


3) Update the example hard-code image referred to on the .gitlab-ci.yml	with your own image variables.

